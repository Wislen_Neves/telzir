package br.com.telzir.testes;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

import br.com.telzir.bean.*;
import br.com.telzir.dao.*;

//Testar a classe TarifaDAO do pacote br.com.telzir.dao
public class TarifaDaoTeste {

	TarifaDAO dao = new TarifaDAO();
	List<Tarifa> listaTarifa = dao.buscaTarifa();

	@Test
	// Testa o tamanho da Lista
	public void tamanhoLista() {

		assertEquals(6, listaTarifa.size());

	}

	@Test
	// Testa valores do objeto t1
	public void validaObjetoT1() {

		assertEquals(11, listaTarifa.get(0).getDddOrigem());
		assertEquals(16, listaTarifa.get(0).getDddDestino());
		assertEquals(1.90, listaTarifa.get(0).getTarifa(), 0.01);

	}

	@Test
	// Testa valores do objeto t2
	public void validaObjetoT2() {

		assertEquals(16, listaTarifa.get(1).getDddOrigem());
		assertEquals(11, listaTarifa.get(1).getDddDestino());
		assertEquals(2.90, listaTarifa.get(1).getTarifa(), 0.01);

	}

	@Test
	// Testa valores do objeto t3
	public void validaObjetoT3() {

		assertEquals(11, listaTarifa.get(2).getDddOrigem());
		assertEquals(17, listaTarifa.get(2).getDddDestino());
		assertEquals(1.70, listaTarifa.get(2).getTarifa(), 0.01);

	}

	@Test
	// Testa valores do objeto t4
	public void validaObjetoT4() {

		assertEquals(17, listaTarifa.get(3).getDddOrigem());
		assertEquals(11, listaTarifa.get(3).getDddDestino());
		assertEquals(2.70, listaTarifa.get(3).getTarifa(), 0.01);

	}

	@Test
	// Testa valores do objeto t5
	public void validaObjetoT5() {

		assertEquals(11, listaTarifa.get(4).getDddOrigem());
		assertEquals(18, listaTarifa.get(4).getDddDestino());
		assertEquals(0.90, listaTarifa.get(4).getTarifa(), 0.01);

	}

	@Test
	// Testa valores do objeto t6
	public void validaObjetoT6() {

		assertEquals(18, listaTarifa.get(5).getDddOrigem());
		assertEquals(11, listaTarifa.get(5).getDddDestino());
		assertEquals(1.90, listaTarifa.get(5).getTarifa(), 0.01);

	}

	@Test
	// Testa um objeto na lista
	public void validaObjetoT7() {

		// Cria novo objeto com valores
		Tarifa t7 = new Tarifa();
		t7.setDddOrigem(11);
		t7.setDddDestino(19);
		t7.setTarifa(0.90);
		listaTarifa.add(t7);

		// Testa valores do novo objeto
		assertEquals(11, listaTarifa.get(6).getDddOrigem());
		assertEquals(19, listaTarifa.get(6).getDddDestino());
		assertEquals(0.90, listaTarifa.get(6).getTarifa(), 0.01);

		// Altera valor DddOrigem do objeto t7 e compara com valor antigo
		listaTarifa.get(6).setTarifa(3.90);
		assertEquals(3.90, listaTarifa.get(6).getTarifa(), 0.01);

	}

}
