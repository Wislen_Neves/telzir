package br.com.telzir.testes;

import br.com.telzir.bean.*;
import static org.junit.Assert.*;

import org.junit.Test;

//Testar a classe Plano do pacote br.com.telzir.bean
public class PlanoTeste {

	@Test
	// Acessar a vari�vel faleMais
	public void recebeFaleMais() {
		Plano plano = new Plano();
		int resultadoPlano = plano.getFaleMais();
		assertEquals(0, resultadoPlano);
	}

	@Test
	// Incluir valor e acessar a vari�vel faleMais
	public void enviaFaleMais() {
		Plano plano = new Plano();
		plano.setFaleMais(25);
		int numero = plano.getFaleMais();
		assertEquals(25, numero);
	}

	@Test
	// Acessar a vari�vel porcAcrescimo
	public void recebePorcAcrescimo() {
		Plano plano = new Plano();
		double resultadoAcrescimo = plano.getPorcAcrescimo();
		assertEquals(0, resultadoAcrescimo, 0.1);
	}

	@Test
	// Incluir valor e acessar a vari�vel porcAcrescimo
	public void enviaPorcAcrescimo() {
		Plano plano = new Plano();
		plano.setPorcAcrescimo(1.5);
		double numero = plano.getPorcAcrescimo();
		assertEquals(1.5, numero, 0.1);
	}

}
