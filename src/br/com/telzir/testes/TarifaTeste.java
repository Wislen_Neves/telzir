package br.com.telzir.testes;

import static org.junit.Assert.*;
import org.junit.Test;
import br.com.telzir.bean.*;

//Testar a classe Tarifa do pacote br.com.telzir.bean
public class TarifaTeste {

	@Test
	// Acessar a vari�vel DddOrigem
	public void recebeDddOrigem() {
		Tarifa tarifa = new Tarifa();
		int resultadoDdd = tarifa.getDddOrigem();
		assertEquals(0, resultadoDdd);
	}

	@Test
	// Incluir valor e acessar a vari�vel DddOrigem
	public void enviaDddOrigem() {
		Tarifa tarifa = new Tarifa();
		tarifa.setDddOrigem(11);
		int numero = tarifa.getDddOrigem();
		assertEquals(11, numero);
	}

	@Test
	// Acessar a vari�vel DddDestino
	public void recebeDddDestino() {
		Tarifa tarifa = new Tarifa();
		int resultadoDdd = tarifa.getDddDestino();
		assertEquals(0, resultadoDdd);
	}

	@Test
	// Incluir valor e acessar a vari�vel DddDestino
	public void enviaDddDestino() {
		Tarifa tarifa = new Tarifa();
		tarifa.setDddDestino(19);
		int numero = tarifa.getDddDestino();
		assertEquals(19, numero);
	}

	@Test
	// Acessar a vari�vel Tarifa
	public void recebeTarifa() {
		Tarifa tarifa = new Tarifa();
		double resultadoTarifa = tarifa.getTarifa();
		assertEquals(0.00, resultadoTarifa, 0.01);
	}

	@Test
	// Incluir valor e acessar a vari�vel DddDestino
	public void enviaTarifa() {
		Tarifa tarifa = new Tarifa();
		tarifa.setTarifa(0.99);
		double numero = tarifa.getTarifa();
		assertEquals(.99, numero, 0.01);
	}

	/*
	 * @Test // Incluir valor e acessar a vari�vel faleMais public void
	 * enviaFaleMais() { Plano plano = new Plano(); plano.setFaleMais(25); int
	 * numero = plano.getFaleMais(); assertEquals(25,numero); }
	 * 
	 * @Test // Acessar a vari�vel porcAcrescimo public void
	 * recebePorcAcrescimo() { Plano plano = new Plano(); double
	 * resultadoAcrescimo = plano.getPorcAcrescimo();
	 * assertEquals(0,resultadoAcrescimo,0.1); }
	 * 
	 * @Test // Incluir valor e acessar a vari�vel porcAcrescimo public void
	 * enviaPorcAcrescimo() { Plano plano = new Plano();
	 * plano.setPorcAcrescimo(1.5); double numero = plano.getPorcAcrescimo();
	 * assertEquals(1.5,numero,0.1); }
	 */

}
