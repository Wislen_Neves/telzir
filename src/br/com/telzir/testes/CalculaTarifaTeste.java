package br.com.telzir.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.telzir.bean.CalculaTarifa;

// Testar a classe CalculaTarifa do pacote br.com.telzir.bean
public class CalculaTarifaTeste {

	CalculaTarifa tarifa = new CalculaTarifa();

	@Test
	// Testa plano DDD11 para DDD16 com os 3 planos
	public void testaCalculoTarifaComPlanoUm() {

		// DDD11 para DDD16 com Fale Mais 30 com 200 minutos de tempo utilizado
		double valorTrinta = tarifa.calculaComPlano(11, 16, 30, 200);
		assertEquals(355.3, valorTrinta, 0.01);

		// DDD11 para DDD16 com Fale Mais 60 com 200 minutos de tempo utilizado
		double valorSessenta = tarifa.calculaComPlano(11, 16, 60, 200);
		assertEquals(292.6, valorSessenta, 0.01);

		// DDD11 para DDD16 com Fale Mais 120 com 200 minutos de tempo utilizado
		double valorCentoEVinte = tarifa.calculaComPlano(11, 16, 120, 200);
		assertEquals(167.2, valorCentoEVinte, 0.01);
	}

	@Test
	// Testa plano DDD16 para DDD11 com os 3 planos
	public void testaCalculoTarifaComPlanoDois() {

		// DDD16 para DDD11 com Fale Mais 30 com 200 minutos de tempo utilizado
		double valorTrinta = tarifa.calculaComPlano(16, 11, 30, 200);
		assertEquals(542.3, valorTrinta, 0.01);

		// DDD16 para DDD11 com Fale Mais 60 com 200 minutos de tempo utilizado
		double valorSessenta = tarifa.calculaComPlano(16, 11, 60, 200);
		assertEquals(446.6, valorSessenta, 0.01);

		// DDD16 para DDD11 com Fale Mais 120 com 200 minutos de tempo utilizado
		double valorCentoEVinte = tarifa.calculaComPlano(16, 11, 120, 200);
		assertEquals(255.2, valorCentoEVinte, 0.01);
	}

	@Test
	// Testa plano DDD11 para DDD17 com os 3 planos
	public void testaCalculoTarifaComPlanoTres() {

		// DDD11 para DDD17 com Fale Mais 30 com 200 minutos de tempo utilizado
		double valorTrinta = tarifa.calculaComPlano(11, 17, 30, 200);
		assertEquals(317.9, valorTrinta, 0.01);

		// DDD11 para DDD17 com Fale Mais 60 com 200 minutos de tempo utilizado
		double valorSessenta = tarifa.calculaComPlano(11, 17, 60, 200);
		assertEquals(261.8, valorSessenta, 0.01);

		// DDD11 para DDD17 com Fale Mais 120 com 200 minutos de tempo utilizado
		double valorCentoEVinte = tarifa.calculaComPlano(11, 17, 120, 200);
		assertEquals(149.6, valorCentoEVinte, 0.01);
	}

	@Test
	// Testa plano DDD17 para DDD11 com os 3 planos
	public void testaCalculoTarifaComPlanoQuatro() {

		// DDD17 para DDD11 com Fale Mais 30 com 200 minutos de tempo utilizado
		double valorTrinta = tarifa.calculaComPlano(17, 11, 30, 200);
		assertEquals(504.9, valorTrinta, 0.01);

		// DDD17 para DDD11 com Fale Mais 60 com 200 minutos de tempo utilizado
		double valorSessenta = tarifa.calculaComPlano(17, 11, 60, 200);
		assertEquals(415.8, valorSessenta, 0.01);

		// DDD17 para DDD11 com Fale Mais 120 com 200 minutos de tempo utilizado
		double valorCentoEVinte = tarifa.calculaComPlano(17, 11, 120, 200);
		assertEquals(237.6, valorCentoEVinte, 0.01);
	}

	@Test
	// Testa plano DDD11 para DDD18 com os 3 planos
	public void testaCalculoTarifaComPlanoCinco() {

		// DDD11 para DDD18 com Fale Mais 30 com 200 minutos de tempo utilizado
		double valorTrinta = tarifa.calculaComPlano(11, 18, 30, 200);
		assertEquals(168.3, valorTrinta, 0.01);

		// DDD11 para DDD18 com Fale Mais 60 com 200 minutos de tempo utilizado
		double valorSessenta = tarifa.calculaComPlano(11, 18, 60, 200);
		assertEquals(138.6, valorSessenta, 0.01);

		// DDD11 para DDD18 com Fale Mais 120 com 200 minutos de tempo utilizado
		double valorCentoEVinte = tarifa.calculaComPlano(11, 18, 120, 200);
		assertEquals(79.2, valorCentoEVinte, 0.01);
	}

	@Test
	// Testa plano DDD18 para DDD11 com os 3 planos
	public void testaCalculoTarifaComPlanoSeis() {

		// DDD18 para DDD11 com Fale Mais 30 com 200 minutos de tempo utilizado
		double valorTrinta = tarifa.calculaComPlano(18, 11, 30, 200);
		assertEquals(355.3, valorTrinta, 0.01);

		// DDD18 para DDD11 com Fale Mais 60 com 200 minutos de tempo utilizado
		double valorSessenta = tarifa.calculaComPlano(18, 11, 60, 200);
		assertEquals(292.6, valorSessenta, 0.01);

		// DDD18 para DDD11 com Fale Mais 120 com 200 minutos de tempo utilizado
		double valorCentoEVinte = tarifa.calculaComPlano(18, 11, 120, 200);
		assertEquals(167.2, valorCentoEVinte, 0.01);
	}

	@Test
	// Testa plano que cobre a quantidade de tempo utilizado
	public void testaCalculoTarifaPlanoCobre() {

		// DDD18 para DDD11 com Fale Mais 30 com 200 minutos de tempo utilizado
		double valorTrinta = tarifa.calculaComPlano(18, 11, 30, 20);
		assertEquals(0, valorTrinta, 0.01);

		// DDD18 para DDD11 com Fale Mais 60 com 200 minutos de tempo utilizado
		double valorSessenta = tarifa.calculaComPlano(18, 11, 60, 20);
		assertEquals(0, valorSessenta, 0.01);

		// DDD18 para DDD11 com Fale Mais 120 com 200 minutos de tempo utilizado
		double valorCentoEVinte = tarifa.calculaComPlano(18, 11, 120, 20);
		assertEquals(0, valorCentoEVinte, 0.01);
	}

	@Test
	// Testa tempo utilizado sem plano
	public void testaCalculoTarifaSemPlano() {

		// DDD11 para DDD16
		double valorSemPlanoUm = tarifa.calculaSemPlano(11, 16, 200);
		assertEquals(380, valorSemPlanoUm, 0.01);

		// DDD16 para DDD11
		double valorSemPlanoDois = tarifa.calculaSemPlano(16, 11, 200);
		assertEquals(580, valorSemPlanoDois, 0.01);

		// DDD11 para DDD17
		double valorSemPlanoTres = tarifa.calculaSemPlano(11, 17, 200);
		assertEquals(340, valorSemPlanoTres, 0.01);

		// DDD17 para DDD11
		double valorSemPlanoQuatro = tarifa.calculaSemPlano(17, 11, 200);
		assertEquals(540, valorSemPlanoQuatro, 0.01);

		// DDD11 para DDD18
		double valorSemPlanoCinco = tarifa.calculaSemPlano(11, 18, 200);
		assertEquals(180, valorSemPlanoCinco, 0.01);

		// DDD18 para DDD11
		double valorSemPlanoSeis = tarifa.calculaSemPlano(18, 11, 200);
		assertEquals(380, valorSemPlanoSeis, 0.01);
	}

	@Test
	// Testa a busca de DDD cadastrado
	public void testaDddExiste() {

		boolean resultado = tarifa.verificaDddExiste(18);

		assertEquals(true, resultado);
	}

	@Test
	// Testa a busca de Plano cadastrado
	public void testaPlanoExiste() {

		boolean resultado = tarifa.verificaPlanoExiste(30);

		assertEquals(true, resultado);
	}

}
