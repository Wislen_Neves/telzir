package br.com.telzir.view;

import java.util.*;

import br.com.telzir.bean.*;

public class InformaTarifa {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		CalculaTarifa calcula = new CalculaTarifa();
		boolean continua = true;
		boolean possuiRegistro = false;
		do {
			try {

				System.out.print("DDD Origem:");
				int dddOrigem = scanner.nextInt();
				// Verifica se o DDD digitado possui promo��o
				possuiRegistro = calcula.verificaDddExiste(dddOrigem);
				if (possuiRegistro == false) {
					throw new IllegalArgumentException();
				}

				System.out.print("DDD Destino:");
				int dddDestino = scanner.nextInt();
				// Verifica se o DDD digitado possui promo��o
				possuiRegistro = calcula.verificaDddExiste(dddDestino);
				if (possuiRegistro == false) {
					throw new IllegalArgumentException();
				}

				System.out.print("Tempo (em minutos):");
				int tempoUtilizado = scanner.nextInt();

				System.out.print("Plano Fale Mais (digite os minutos):");
				int planoFale = scanner.nextInt();
				// Verifica se o Plano existe
				possuiRegistro = calcula.verificaPlanoExiste(planoFale);
				if (possuiRegistro == false) {
					throw new IllegalArgumentException();
				}

				System.out.println(" ");
				System.out.println("Origem: " + dddOrigem);
				System.out.println("Destino: " + dddDestino);
				System.out.println("Tempo informado: " + tempoUtilizado);
				System.out.println("Plano Fale Mais: " + planoFale);
				System.out.println("");
				System.out.println("Valor com Fale Mais: "
						+ calcula.calculaComPlano(dddOrigem, dddDestino,
								planoFale, tempoUtilizado));
				System.out.println("Valor sem Fale Mais: "
						+ calcula.calculaSemPlano(dddOrigem, dddDestino,
								tempoUtilizado));

				continua = false;

			} catch (InputMismatchException e1) {
				System.err
						.println("Os campos aceitam apenas n�meros, tente novamente.");
				scanner.nextLine();
				System.out.println(" ");

			} catch (IllegalArgumentException e2) {
				System.err
						.println("Dado informado n�o existe ou n�o est� cadastrado. Informe os dados novamente");
				scanner.nextLine();
				System.out.println(" ");
			}

		} while (continua == true);
		scanner.close();

	}

}
