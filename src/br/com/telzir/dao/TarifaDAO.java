package br.com.telzir.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.telzir.bean.Plano;
import br.com.telzir.bean.Tarifa;

public class TarifaDAO {

	// Construtor
	public TarifaDAO() {

	}

	// busca tarifa
	public List<Tarifa> buscaTarifa() {

		// Objetos Tarifa
		Tarifa t1 = new Tarifa();
		Tarifa t2 = new Tarifa();
		Tarifa t3 = new Tarifa();
		Tarifa t4 = new Tarifa();
		Tarifa t5 = new Tarifa();
		Tarifa t6 = new Tarifa();

		// Valores Tarifa 1
		t1.setDddOrigem(11);
		t1.setDddDestino(16);
		t1.setTarifa(1.90);

		// Valores Tarifa 2
		t2.setDddOrigem(16);
		t2.setDddDestino(11);
		t2.setTarifa(2.90);

		// Valores Tarifa 3
		t3.setDddOrigem(11);
		t3.setDddDestino(17);
		t3.setTarifa(1.70);

		// Valores Tarifa 4
		t4.setDddOrigem(17);
		t4.setDddDestino(11);
		t4.setTarifa(2.70);

		// Valores Tarifa 5
		t5.setDddOrigem(11);
		t5.setDddDestino(18);
		t5.setTarifa(0.90);

		// Valores Tarifa 6
		t6.setDddOrigem(18);
		t6.setDddDestino(11);
		t6.setTarifa(1.90);

		List<Tarifa> listaTarifa = new ArrayList<Tarifa>();
		listaTarifa.add(t1);
		listaTarifa.add(t2);
		listaTarifa.add(t3);
		listaTarifa.add(t4);
		listaTarifa.add(t5);
		listaTarifa.add(t6);

		return listaTarifa;

	}

	// Lista plano
	public List<Plano> buscaPlano() {

		Plano p1 = new Plano();
		p1.setFaleMais(30);
		p1.setPorcAcrescimo(0.10);

		Plano p2 = new Plano();
		p2.setFaleMais(60);
		p2.setPorcAcrescimo(0.10);

		Plano p3 = new Plano();
		p3.setFaleMais(120);
		p3.setPorcAcrescimo(0.10);

		List<Plano> listaPlano = new ArrayList<Plano>();
		listaPlano.add(p1);
		listaPlano.add(p2);
		listaPlano.add(p3);

		return listaPlano;

	}

}
