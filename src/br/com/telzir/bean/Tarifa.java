package br.com.telzir.bean;

//Classe que manipula dados de tarifa
public class Tarifa {

	private int dddOrigem;
	private int dddDestino;
	private double tarifa;

	public int getDddOrigem() {
		return dddOrigem;
	}

	public void setDddOrigem(int dddOrigem) {
		this.dddOrigem = dddOrigem;
	}

	public int getDddDestino() {
		return dddDestino;
	}

	public void setDddDestino(int dddDestino) {
		this.dddDestino = dddDestino;
	}

	public double getTarifa() {
		return tarifa;
	}

	public void setTarifa(double tarifa) {
		this.tarifa = tarifa;
	}

}
