/**
 * 
 * Programa para calular valores do plano Fale Mais
 * 
 * @author Wislen Neves 
 * 
 * Construido em 17/04/2014
 * 
 */

package br.com.telzir.bean;

import java.math.*;
import java.util.List;
import br.com.telzir.dao.*;

public class CalculaTarifa {

	// Inst�ncia das listas da classe TarifaDAO
	TarifaDAO dao = new TarifaDAO();
	List<Tarifa> listaTarifa = dao.buscaTarifa();
	List<Plano> listaPlano = dao.buscaPlano();

	// Calcula valor com plano Fale Mais
	public double calculaComPlano(int dddOrigem, int dddDestino, int planoFale,
			int tempoUtilizado) {

		double tarifaPlano = 0, valorComPlano = 0, porcAcrescimo = 0;
		// Busca o valor da tarifa, de acordo com os DDD�s informados
		for (int i = 0; i < listaTarifa.size(); i++) {
			if (dddOrigem == listaTarifa.get(i).getDddOrigem()
					&& dddDestino == listaTarifa.get(i).getDddDestino()) {
				tarifaPlano = listaTarifa.get(i).getTarifa();
				break;
			}
		}

		// O adicional, de acordo com o plano indicado
		for (int i = 0; i < listaPlano.size(); i++) {
			if (planoFale == listaPlano.get(i).getFaleMais()) {
				porcAcrescimo = (listaPlano.get(i).getPorcAcrescimo() + 1);
				break;
			}
		}

		// Calculo do valor com plano
		if (planoFale >= tempoUtilizado) {
			valorComPlano = 0;
		} else {

			valorComPlano = (((tempoUtilizado - planoFale) * tarifaPlano) * porcAcrescimo);
		}
		BigDecimal resultadoComPlano = new BigDecimal(valorComPlano).setScale(
				3, RoundingMode.HALF_EVEN);
		return resultadoComPlano.doubleValue();
	}

	// Calcula resultado sem o plano Fale Mais
	public double calculaSemPlano(int dddOrigem, int dddDestino,
			int tempoUtilizado) {
		double valorSemPlano = 0, tarifaPlano = 0;
		// Busca o valor da tarifa, de acordo com os DDD�s informados
		for (int i = 0; i < listaTarifa.size(); i++) {
			if (dddOrigem == listaTarifa.get(i).getDddOrigem()
					&& dddDestino == listaTarifa.get(i).getDddDestino()) {
				tarifaPlano = listaTarifa.get(i).getTarifa();
				break;
			}
		}
		valorSemPlano = tempoUtilizado * tarifaPlano;
		BigDecimal resultadoSemPlano = new BigDecimal(valorSemPlano).setScale(
				3, RoundingMode.HALF_EVEN);
		return resultadoSemPlano.doubleValue();
	}

	// Verifica se DDD Informado existe promo��o
	public boolean verificaDddExiste(int dddInformado) {
		boolean igualTarifa = false;
		for (Tarifa tarifa : listaTarifa) {
			if (dddInformado == tarifa.getDddOrigem()) {
				igualTarifa = true;
				break;
			}
		}
		return igualTarifa;
	}

	// Verifica se Plano Informado existe
	public boolean verificaPlanoExiste(int planoInformado) {
		boolean igualPlano = false;
		for (Plano plano : listaPlano) {
			if (planoInformado == plano.getFaleMais()) {
				igualPlano = true;
				break;
			}
		}
		return igualPlano;
	}
}
