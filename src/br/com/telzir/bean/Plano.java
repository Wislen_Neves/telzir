package br.com.telzir.bean;

//Classe que manipula dados do plano
public class Plano {

	private int faleMais;
	private double porcAcrescimo; // Caso haja porcentagem diferencida no futuro

	public int getFaleMais() {
		return faleMais;
	}

	public void setFaleMais(int faleMais) {
		this.faleMais = faleMais;
	}

	public double getPorcAcrescimo() {
		return porcAcrescimo;
	}

	public void setPorcAcrescimo(double porcAcrescimo) {
		this.porcAcrescimo = porcAcrescimo;
	}

}
